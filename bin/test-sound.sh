#! /bin/bash


for a in $(aplay -L | sed -n '/^[^ ]/p'); do 

    echo Device $a - Testing
    speaker-test -D$a -c2 -t sine -l 1 2>&1 >/dev/null

done

