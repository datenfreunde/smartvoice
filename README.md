# Smartvoice

Smart Service Text-To-Speech Container


Based on Alpine Linux 3.5, Python 3.6.5, ffmpeg 4.0

Currently available: Sound test

```
# Build the container

make build

# Make sure you hear the sound on the docker host
# cycles through all available devices. 

make test-sound-local

# Test sound from inside a docker container
# cycles through all available devices. 

make test-sound-docker



```

- work in progress -
