SHELL := /bin/bash


build:
	docker build -t smartvoice .

test-sound-local:
	bin/test-sound.sh

test-sound-docker:
	 docker run -it --rm --device /dev/snd smartvoice bin/test-sound.sh
